# v0.3.0 2020-12-03

Custom defaults added by `set_default` are now constraint checked, as this might cause
backwards incompatibilities I raised the minor versiou number

# v0.2.1 2020-11-27

Add constraint `:symbol`

# v0.2.0 2020-11-25

All constraints implemented

# v0.1.5 2020-11-22

`Constraint#default` accepts a block à la `Hash#fetch`

# v0.1.4 2020-11-20

implemented: :positive_int

# v0.1.3 2020-11-20

constraints added: :int and :string

predefined: :number

# v0.1.2 2020-11-20

Expose name and default of a Constraint

Document equality behavior
