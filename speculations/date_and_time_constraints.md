# Lab42::BasicConstraints

## Speculating about Date and Time Constraints

Given we alias `Lab42::BasicConstraints` as `BC` 

```ruby
  require "lab42/basic_constraints/alias"
  def assert_error value, with_message=nil
    value in [error, message]
    if with_message
      expect( message ).to eq(with_message)
    end
    expect( error ).to eq(BC::ConstraintError)
  end
```


### Context `:date`

Given a `:date` constraint

```ruby
    let(:date) {BC.date}
```

Then legal dates will not cause any trouble

```ruby
    date.(Date.today.iso8601) in [:ok, nil]
```
But illegal will

```ruby
    assert_error(date.("1900-02-29"), "\"1900-02-29\" is not a legal date")
```

And the default is today of course

```ruby
    Timecop.freeze do
      expect( date.default ).to eq(Date.today.iso8601)
    end
```

And today is defined by the call, not the definition of `date` 

```ruby
    Timecop.freeze(Date.new(2525, 12, 24)) do
      expect( date.default ).to eq("2525-12-24")
    end
```

### Context `:date` with custom default

Given we define a default date ourselves

```ruby
    let(:date) { BC.date(default: "3535-01-01")}
```

Then this gets us the new default of course

```ruby
    expect(date.default).to eq("3535-01-01")
```

And that even if we time travel

```ruby
    Timecop.freeze(Date.new(2525, 12, 24)) do
      expect( date.default ).to eq("3535-01-01")
    end
```

### Context `:date_time`

Given a `:date_time`
```ruby
    let(:date_time) {BC.date_time}
```

Then legal dates will not cause any trouble

```ruby
    date_time.(DateTime.now.iso8601) in [:ok, nil]
```
But illegal will

```ruby
    assert_error(date_time.("1900-02-29"), "\"1900-02-29\" is not a legal date_time")
```

And the default is now (right now as defined in Spaceballs) of course

```ruby
    Timecop.freeze do
      expect( date_time.default ).to eq(DateTime.now.iso8601)
    end
```
### Context `:date_time` with custom default

Given we define a default date ourselves

```ruby
    let(:date_time) { BC.date_time(default: "3535-01-01")}
```

Then this gets us the new default of course

```ruby
    expect(date_time.default).to eq("3535-01-01")
```

And that even if we time travel

```ruby
    Timecop.freeze(Date.new(2525, 12, 24)) do
      expect( date_time.default ).to eq("3535-01-01")
    end
```
### Context `:day`

Given a `:day`
```ruby
    let(:day) {BC.day}
```

Then 1 is a legal value
```ruby
    day.(1) in [:ok, nil]
```

And so is 31
```ruby
    day.(31) in [:ok, nil]
```

And are all values in between
```ruby
    100.times do
      day.((2..30).sample) in [:ok, nil]
    end
```

But strings are not
```ruby
    assert_error(day.("1"))
```

And nor is -1
```ruby
    assert_error(day.(-1))
```
And not even 32
```ruby
    assert_error(day.(32))
```

### Context `:hour`

Given a `:hour`
```ruby
    let(:hour) {BC.hour}
```

Then 0 is a legal value
```ruby
    hour.(0) in [:ok, nil]
```

And so is 23
```ruby
    hour.(23) in [:ok, nil]
```

And are all values in between
```ruby
    100.times do
      hour.((1..22).sample) in [:ok, nil]
    end
```

But strings are not
```ruby
    assert_error(hour.("1"))
```

And nor is -1
```ruby
    assert_error(hour.(-1))
```
And not even 24
```ruby
    assert_error(hour.(60))
```
### Context `:minute`

Given a `:minute`
```ruby
    let(:minute) {BC.minute}
```

Then 0 is a legal value
```ruby
    minute.(0) in [:ok, nil]
```

And so is 59
```ruby
    minute.(59) in [:ok, nil]
```

And are all values in between
```ruby
    100.times do
      minute.((1..58).sample) in [:ok, nil]
    end
```

But strings are not
```ruby
    assert_error(minute.("1"))
```

And nor is -1
```ruby
    assert_error(minute.(-1))
```
And not even 6O
```ruby
    assert_error(minute.(60))
```
### Context `:month`

Given a `:month`
```ruby
    let(:month) {BC.month}
```

Then 1 is a legal value
```ruby
    month.(1) in [:ok, nil]
```

And so is 12
```ruby
    month.(12) in [:ok, nil]
```

And are all values in between
```ruby
    100.times do
      month.((2..11).sample) in [:ok, nil]
    end
```

But strings are not
```ruby
    assert_error(month.("1"))
```

And nor is 0 
```ruby
    assert_error(month.(0))
```
And not even 13
```ruby
    assert_error(month.(13))
```

### Context `:second`

Given a `:second`
```ruby
    let(:second) {BC.second}
```

Then 0 is a legal value
```ruby
    second.(0) in [:ok, nil]
```

And so is 59
```ruby
    second.(59) in [:ok, nil]
```

And are all values in between
```ruby
    100.times do
      second.((1..58).sample) in [:ok, nil]
    end
```

But strings are not
```ruby
    assert_error(second.("1"))
```

And nor is -1
```ruby
    assert_error(second.(-1))
```
And not even 6O
```ruby
    assert_error(second.(60))
```
### Context `:year` 

Given a `:year` constraint

```ruby
    let(:year) {BC.year}
```

Then icode`2020` is ok

```ruby
  year.(2020) in [:ok, nil]
    
```

