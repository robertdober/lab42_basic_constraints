# Lab42::BasicConstraints

## Speculating about String Constraints

Given we alias `Lab42::BasicConstraints` as `BC` 

```ruby
  require "lab42/basic_constraints/alias"
  def assert_error value, with_message=nil
    value in [error, message]
    if with_message
      expect( message ).to eq(with_message)
    end
    expect( error ).to eq(BC::ConstraintError)
  end

  def assert_ok(value)
    value in [:ok, nil]
  end
```

### Context `:alphanumeric_string`

Given an `:alphanumeric_string`
```ruby
    let(:alpha_string) {BC.alphanumeric_string}
    let(:lim_alpha_string){BC.alphanumeric_string(size: 1..2)}
```
Then the gooood still please
```ruby
    assert_ok(alpha_string.("7ac"))
    assert_ok(alpha_string.("7éç"))
    assert_ok(alpha_string.(""))
    assert_error(alpha_string.("&"), "\"&\" is not a legal alphanumeric_string")
```
And the same holds for limits
```ruby
    assert_ok(lim_alpha_string.("ée"))
    assert_ok(lim_alpha_string.("é"))
    assert_error(lim_alpha_string.(""), "\"\" is not a legal alphanumeric_string")
```

### Context `:limited_string` 

limits the size of a string

Given these limited strings
```ruby
    let(:non_empty) {BC.limited_string(min: 1)}
    let(:empty) {BC.limited_string(size: 0..0)} # Stupid I know
    let(:middle){BC.limited_string(min: 2, max: 3)}
```
Then we shall check with `non_empty` 
```ruby
    assert_ok(non_empty.("a"))
    assert_ok(non_empty.(" "))
    assert_ok(non_empty.("12"))
    assert_error(non_empty.(""))
    assert_error(non_empty.(12))
```
And so we shall do the same with `empty` 
```ruby
    assert_ok(empty.(""))
    assert_error(empty.("1"))
    assert_error(empty.(nil))
    assert_error(empty.([]))
```
And now let us take the road down the `middle` 
```ruby
    assert_ok(middle.("aa"))
    assert_ok(middle.("bb"))
    assert_ok(middle.("ሴ⍅癔"))
    assert_error(middle.("a"))
    assert_error(middle.("♥"))
    assert_error(middle.(""))
    assert_error(middle.("1234"))
    assert_error(middle.(true))
```
### Context `:lowercase_string`

Given a `:lowercase_string`
```ruby
    let(:lowercase_string) {BC.lowercase_string}
```
Then small is beautiful
```ruby
    assert_ok(lowercase_string.(""))
```
And a little bit less smaller is ok
```ruby
    assert_ok(lowercase_string.("a"))
```
And of course (I mean finally) we master UTF8
```ruby
    assert_ok(lowercase_string.("héllo"))
```
But watch out for capitals
```ruby
    assert_error(lowercase_string.("Héllo"))
```
And UTF8 again
```ruby
    assert_error(lowercase_string.("hÉllo"))
```

And non letters do not matter
```ruby
    assert_ok(lowercase_string.("lab 42"))
```

### Context `:non_empty_string`

Given a `:non_empty_string`
```ruby
    let(:non_empty_string) {BC.non_empty_string}
```
### Context `string` 

Given a `:string` constraint

```ruby
    let(:string) {BC.string}
```

Then it checks
```ruby
    assert_ok(string.("42"))
    string.(42) in [error, message]
    expect( error ).to eq(BC::ConstraintError)
```
And it has no predefined default
```ruby
    expect{ string.default }
      .to raise_error(
      BC::MissingDefaultError,
      "Constraint string has no predefined default")
```
### Context `:uppercase_string`

Given a `:uppercase_string`
```ruby
    let(:uppercase_string) {BC.uppercase_string}
```
Then small is beautiful
```ruby
    assert_ok(uppercase_string.(""))
```
But smaller is not good
```ruby
    assert_error(uppercase_string.("a"), %{"a" is not a legal uppercase_string})
```
And we shall try larger
```ruby
    assert_ok(uppercase_string.("R"))
```
And of course (I mean finally) we master UTF8
```ruby
    assert_ok(uppercase_string.("HÉLLO"))
```
But watch out for small ones
```ruby
    assert_error(uppercase_string.("HéLLO"))
```

And non letters do not matter
```ruby
    assert_ok(uppercase_string.("LAB 42"))
```
