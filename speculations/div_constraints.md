# Lab42::BasicConstraints

## Speculating about Diverse Constraints

Given we alias `Lab42::BasicConstraints` as `BC` 

```ruby
  require "lab42/basic_constraints/alias"
  def assert_error value, with_message=nil
    value in [error, message]
    if with_message
      expect( message ).to eq(with_message)
    end
    expect( error ).to eq(BC::ConstraintError)
  end

  def assert_ok(value)
    value in [:ok, nil]
  end
```


### Context `:bool`

Given a `:bool` constraint

```ruby
    let(:bool) {BC.bool}
```

Then `true` is ok

```ruby
   bool.(true) in [:ok, nil]
```

And `false` is ok

```ruby
   bool.(false) in [:ok, nil]
```

But `nil` ain't
```ruby
  
   assert_error(bool.(nil), "nil is not a legal bool")
```

### Context `int` 

Given an `:int` constraint

```ruby
    let(:int) {BC.int}
```

Then it checks
```ruby
    int.(42) in [:ok, nil]
    int.("42") in [error, message]
    expect( error ).to eq(BC::ConstraintError)
```
And it has no predefined default
```ruby
    expect{ int.default }
      .to raise_error(
      BC::MissingDefaultError,
      "Constraint int has no predefined default")
```
But the default can be set of course
```ruby
    expect( int.set_default(42).default ).to eq(42)
```

### Context `:int_range` 

Given some `:int_range` constraints

```ruby
  let(:positives) {BC.int_range(min: 1)}
  let(:naturals) {BC.int_range(min: 0)}
  let(:negatives) {BC.int_range(min: -Float::INFINITY, max: -1)}
  let(:digits) {BC.int_range(range: 0..9)}
```

Then some checks, obviously fail ;)
```ruby
    assert_error(positives.(0))
    assert_error(positives.(-42))
    assert_error(naturals.(-1))
    assert_error(negatives.(0))
    assert_error(digits.(-1))
    assert_error(digits.(10))
```
But some checks succeed
```ruby
    assert_ok(positives.(1))
    assert_ok(positives.(42))
    assert_ok(naturals.(0))
    assert_ok(naturals.(42))
    assert_ok(naturals.(1_000_000_000_000))
    assert_ok(negatives.(-1_000_000_000_000))
    assert_ok(negatives.(-1))
    (0..9).each {assert_ok(digits.(_1))}
```

### Context `non_negative_int` 

Given a `non_negative_int` 
```ruby
    let(:nni) {BC.non_negative_int}
```
Then it likes 0
```ruby
    assert_ok(nni.(0))
```
And it likes some greater numbers
```ruby
    assert_ok(nni.(42))
```
But it dislikes some small and smaller numbers
```ruby
    assert_error(nni.(-1))
    assert_error(nni.(-42))
    assert_error(nni.(-1_000_000_000_000))
```
And it even dislikes some int likes
```ruby
    assert_error(nni.(1.0))
    assert_error(nni.(12r))
```

And it has a predefined default
```ruby
  expect( nni.default ).to eq(0)
```
### Context `positive_int` 

Given a `positive_int` 
```ruby
    let(:pos) {BC.positive_int}
```
Then it likes 1
```ruby
    assert_ok(pos.(1))
```
And it likes some greater numbers
```ruby
    assert_ok(pos.(42))
```
But it dislikes some small and smaller numbers
```ruby
    assert_error(pos.(-1))
    assert_error(pos.(-42))
    assert_error(pos.(-1_000_000_000_000))
```
And it even dislikes some int likes
```ruby
    assert_error(pos.(1.0))
    assert_error(pos.(12r))
```

And it has a predefined default
```ruby
  expect( pos.default ).to eq(1)
```
### Context `:non_negative_number`

Given a `:non_negative_number`
```ruby
    let(:nnn) {BC.non_negative_number}
```
Then we can check ints, floats and many more (1 to be exact)
```ruby
    assert_ok(nnn.(0))
    assert_ok(nnn.(0.0))
    assert_ok(nnn.(0r))
    assert_ok(nnn.(1e-10))
```
And we will fail some of them
```ruby
    assert_error(nnn.(-1))
    assert_error(nnn.(-1r))
    assert_error(nnn.(-(1e-10)))
    assert_error(nnn.(1i)) # Complex numbers cannot be compared like that
    assert_error(nnn.(nil))
```
And it does not have a default
```ruby
    expect{to nnn.default}
      .to raise_error(BC::MissingDefaultError)
```

### Context `:number`

Given a `:number`
```ruby
    let(:number) {BC.number}
```
Then many things are numbers
```ruby
    assert_ok(number.(0))
    assert_ok(number.(0.1))
    assert_ok(number.(1/2r))
    assert_ok(number.(1i))
```
But many things are not
```ruby
    assert_error(number.(nil))
    assert_error(number.("42"))
    assert_error(number.([1]))
```
### Context `:positive_float`

Given a `:positive_float`
```ruby
    let(:positive_float) {BC.positive_float}
```
### Context `:positive_int`

Given a `:positive_int`
```ruby
    let(:positive_int) {BC.positive_int}
```
### Context `:positive_number`

Given a `:positive_number`
```ruby
    let(:positive_number) {BC.positive_number}
```

### Context `:symbol`

Given a `:symbol`
```ruby
    let(:symbol) {BC.symbol}
```

Then symbols are just fine
```ruby
    assert_ok(symbol.(:symbol))
```
And strings are not :O
```ruby
    assert_error(symbol.("symbol"))
```
