# Lab42::BasicConstraints

## Speculating about Diverse Constraints

### Context without predefined defaults

Given a constraint without a predefined default
```ruby
    let(:an_int) {BC.int}
    let(:sentinel) {Object.new}
```

Then we cannot access its default
```ruby
    expect{ an_int.default }
    .to raise_error(BC::MissingDefaultError,"Constraint int has no predefined default")
```

But we can execute a block instead of getting an Exception rosen
```ruby
    expect( an_int.default {sentinel} ).to eq(sentinel)
```

And we can also set a default ourselves
```ruby
    an_int.set_default 42
    expect( an_int.default ).to eq(42)
```

### Context with predefined defaults

Given a constraint with a predefined default
```ruby
    let (:a_natural) {BC.non_negative_int}
```
Then it can be accessed w/o problem of course
```ruby
    expect( a_natural.default ).to be_zero
```
And even a block does not affect that result
```ruby
    expect( a_natural.default {sentinel} ).to be_zero
```
But we can override this default
```ruby
    expect( a_natural.set_default(42).default ).to eq(42)
```

### Context Constraint Checks when setting defaults

Given the same constraint again
```ruby
    let (:a_natural) {BC.non_negative_int}
    let(:sentinel) { "sentinel"}
```

And we need to be careful when we are setting a default
```ruby
    expect{ 
      a_natural.set_default sentinel
    }.to raise_error(BC::ConstraintError, %{"sentinel" is not a legal non_negative_int}) 
```
