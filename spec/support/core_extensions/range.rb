require_relative "array"
class Range
  def sample
    case first
    when Integer
      rand(self)
    else
      to_a.sample
    end
  end
end
