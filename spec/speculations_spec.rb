require "speculate_about"
RSpec.describe "Speculations" do
  context "main speculation" do
    speculate_about "README.md", alternate_syntax: true
  end

  context "all detailed speculations" do
    context "more_about_defaults" do
      speculate_about "./speculations/#{description}.md", alternate_syntax: true
    end

    context "date and time constraint" do
      speculate_about "./speculations/date_and_time_constraints.md", alternate_syntax: true
    end

    context "diverse constraints" do
      speculate_about "./speculations/div_constraints.md", alternate_syntax: true
    end

    context "string constraints" do
      speculate_about "./speculations/string_constraints.md", alternate_syntax: true
    end
  end

end
