module Lab42
  module BasicConstraints
    module Helpers
      module RangeHelper extend self

        def make_range(range: nil, min: nil, max: nil)
          return unless range || min || max
          raise ArgumentError, "cannot provide min or max with range" if
          range && (min || max)
          range || _make_min_max_range(min, max) 
        end

        def make_range!(range: nil, min: nil, max: nil)
          make_range(range: range, min: min, max: max)
            .tap do |range|
              raise ArgumentError, "Must provide either range or min or max" unless range
            end
        end

        private
        def _make_min_max_range(min, max)
          min ||= 0
          max ||= Float::INFINITY
          min..max
        end
      end
    end
  end
end
