require "lab42/result"
Result = Lab42::Result
module Lab42
  module BasicConstraints
    ConstraintError = Class.new RuntimeError
    MissingDefaultError = Class.new RuntimeError
    class Constraint
      attr_reader :default, :name

      def call value
        return Result.ok if @constraint.(value)
        Result.error("#{_show value} is not a legal #{@name}", error: Lab42::BasicConstraints::ConstraintError)
      end

      def default &blk
        if @has_default
          case @default
          when Proc
            @default.()
          else
            @default
          end
        else
          return blk.() if blk
          raise Lab42::BasicConstraints::MissingDefaultError, "Constraint #{name} has no predefined default"
        end
      end

      def set_default value=nil, &blk
        @default = 
          if value
            value
          else
            blk
          end
        @has_default = true
        raise Lab42::BasicConstraints::ConstraintError, "#{_show @default} is not a legal #{@name}" unless
          @constraint.(default)
        self
      end

      private
      def _show value
        case value
        when NilClass
          "nil"
        when String
          value.inspect
        else
          value
        end
      end

      def initialize name, &blk
        @constraint = blk
        @has_default = false
        @default = -> {}
        @name = name
      end
    end
  end
end
require_relative "constraints/int"
