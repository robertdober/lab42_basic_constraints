require_relative "constraint" 
require_relative "helpers/range_helper"
module Lab42
  module BasicConstraints

    module Implementation extend self

      IntConstraints = {
        day: 1..31,
        hour: 0..23,
        minute: 0..59,
        month: 1..12,
        second: 0..59
      }
      IntConstraints.each do |constraint_name, constraint_range|
        define_method constraint_name do 
          Constraint::IntConstraint.new(constraint_name, range: constraint_range)
        end
      end

      def int_range(range: nil, min: nil, max: nil)
        Constraint::IntConstraint.new(:int_range, range: range, min: min, max: max)
      end

      StringConstraints = {
        all_digits_string: %r{\A\d*\z},
        alphanumeric_string: %r{\A[[:alnum:]]*\z}
      }

      StringConstraints.each do |constraint_name, constraint_rgx|
        define_method constraint_name do |size: nil, min: nil, max: nil|
          _regex_constraint(constraint_name, constraint_rgx, size: size, min: min, max: max)
        end
      end

      def date default: nil
        require "date"
        Constraint.new(:date) do |value|
          Date.parse(value) rescue false
        end
          .set_default default do
          Date.today.iso8601
        end
      end

      def date_time default: nil
        require "date"
        Constraint.new(:date_time) do |value|
          DateTime.parse(value) rescue false
        end
          .set_default default do
          DateTime.now.iso8601
        end
      end

      def limited_string size: nil, min: nil, max: nil
        size_range = Helpers::RangeHelper.make_range!(range: size, min: min, max: max)
        Constraint.new(:limited_string) do |value|
          String === value && size_range === value.size
        end
      end

      def lowercase_string
        Constraint.new :lowercase_string do |value|
          String === value && value.downcase == value
        end
      end

      def non_negative_int
        Constraint::IntConstraint.new(:non_negative_int, min: 0)
          .set_default(0)
      end

      def non_negative_number
        Constraint.new :non_negative_number do |value|
          Numeric === value && !(Complex === value) && value >= 0
        end
      end

      def positive_int
        Constraint::IntConstraint.new(:positive_int, min: 1)
          .set_default(1)
      end

      def positive_number
        Constraint.new :positive_number do |value|
          Numeric === value && value > 0
        end
      end

      def uppercase_string
        Constraint.new :uppercase_string do |value|
          String === value && value.upcase == value
        end
      end

      def year
        Constraint.new(:year) do |value|
          Integer === value
        end
          .set_default do 
            Time.now.utc.year.to_i
        end
      end


      private

      def _regex_constraint(name, rgx, size: nil, min: nil, max: nil)
        range_size = Helpers::RangeHelper.make_range(range: size, min: min, max: max)
        if range_size
          Constraint.new(name) do |value|
            String === value && range_size === value.size && rgx === value
          end
        else
          Constraint.new(name) do |value|
            String === value && rgx === value
          end
        end
      end
    end
  end
end
