require_relative "../helpers/range_helper.rb"
module Lab42
  module BasicConstraints
    class Constraint
      class IntConstraint < Constraint

        private
        def initialize name, range: nil, min: nil, max: nil
          super(name)
          range = Helpers::RangeHelper.make_range!(range: range, min: min, max: max)
          @constraint = -> (value) {
            Integer === value && range === value
          }
        end
      end
    end
  end
end

