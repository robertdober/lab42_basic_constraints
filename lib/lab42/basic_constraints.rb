require_relative "basic_constraints/implementation.rb"
module Lab42
  module BasicConstraints extend self
    Constraints = {
       all_digits_string: :all_digits_string,
       alphanumeric_string: :alphanumeric_string,

       bool: ->(x){[false, true].include? x},

       date: :date,
       date_time: :date_time,
       day: :day,

       hour: :hour,

       int: Integer,
       int_range: :int_range,

       limited_string: :limited_string,
       lowercase_string: :lowercase_string,

       minute: :minute,
       month: :month,
    
       non_negative_int: :non_negative_int,
       non_negative_number: :non_negative_number,
       number: Numeric,

       positive_int: :positive_int,
       positive_number: :positive_number,

       second: :second,
       string: String,
       symbol: Symbol,

       uppercase_string: :uppercase_string,

       year: :year
    }
    def all_constraints
      Constraints.keys
    end

    def from_symbol name, *args, **kwds, &blk
      cons = Constraints.fetch(name, &blk)
      case cons
      # when Constraint
      #   cons
      when Proc
        Constraint.new(name, &cons)
      when Symbol
        Implementation.send(cons, *args, **kwds)
      when Class
        Constraint.new(name){ cons === _1 }
      end
    end

    def method_missing(name, *args, **kwds)
      from_symbol(name, *args, **kwds) {super}
    end
  end
end
