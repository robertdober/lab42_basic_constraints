$:.unshift( File.expand_path( "../lib", __FILE__ ) )
require "lab42/basic_constraints/version"
version = Lab42::BasicConstraints::VERSION
Gem::Specification.new do |s|
  s.name        = 'lab42_basic_constraints'
  s.version     = version
  s.summary     = 'Basic, useful and recurring constraints'
  s.description = %{Constraints like :positive_int, :non_empty_string, ...}
  s.authors     = ["Robert Dober"]
  s.email       = 'robert.dober@gmail.com'
  s.files       = Dir.glob("lib/**/*.rb")
  s.files      += %w{LICENSE README.md}
  s.homepage    = "https://bitbucket.org/robertdober/lab42_basic_constraints"
  s.licenses    = %w{Apache-2.0}

  s.required_ruby_version = '>= 2.7.0'
  s.add_dependency 'lab42_result', '~> 0.1'

  s.add_development_dependency 'pry', '~> 0.10'
  s.add_development_dependency 'pry-byebug', '~> 3.9'
  s.add_development_dependency 'rspec', '~> 3.10'
  s.add_development_dependency 'timecop', '~> 0.9.2'
  # s.add_development_dependency 'travis-lint', '~> 2.0'
end
